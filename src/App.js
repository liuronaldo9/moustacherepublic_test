import React from 'react';
import { prop, filter, isEmpty } from 'ramda';
import Header from './common/header';
import Detail from './pages/detail';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.setCart = this.setCart.bind(this);
    this.getCart = this.getCart.bind(this);
    this.state = {
      setCart: this.setCart,
      getCart: this.getCart,
      cart: []
    };
  }

  setCart(product) {
    if (product.size) {
      let currentCart = this.state.cart;
      let productSize = product.size;

      const existingProduct = filter(
        item => prop('size')(item) === productSize
      )(currentCart);

      if (isEmpty(existingProduct)) {
        currentCart.push({ size: productSize, quantity: 1 });
      } else {
        currentCart.map(item => {
          if (item.size === productSize) {
            item.quantity = item.quantity + 1;
          }
          return item.quantity
        });
      }
      this.setState({ cart: currentCart });
    } else {
      alert('YOU MUST SELECT SIZE');
    }
  }

  getCart() {
    return this.state.cart;
  }

  render() {
    return (
      <React.Fragment>
        <Header getCart={this.getCart} />
        <Detail setCart={this.setCart} />
      </React.Fragment>
    );
  }
}

export default App;
