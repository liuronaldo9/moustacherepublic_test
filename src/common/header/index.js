import React from 'react';
import { isEmpty } from 'ramda';
import './header.css';
import image from '../../img/classic-tee.jpg';

export default class Header extends React.Component {
  render() {
    const { getCart } = this.props;
    // console.log('getCART', getCart());
    const cartQuantity = getCart().map(e => {
      return e.quantity;
    }) 
    let totalQuantity = function(ar){   
      if (ar.length !== 0) {
        let sum=ar.reduce(function(prev, curr){
          return prev + curr;
        })
        return  sum;
      }
    }
    let cartInfo = null;
    if (!isEmpty(getCart())) {
      cartInfo = (
        getCart().map((item, key) => {
          return (
            <div className="cart-block" key={key}>
              <div className="cart-left" >
                <div className="cart-pic">
                  <img src={ image } alt="" />
                </div>
              </div>
              <div className="cart-right">
                <p className="title">Classic Tee</p>
                <div className="price">
                  <span>{ item.quantity }x </span>
                  $75.00
                </div>
                <div>
                  <p className="info-size">
                    <label>SIZE: { item.size }</label>
                  </p>
                </div>
              </div>
            </div>
          )
        })
      )
    }
    return (
      <div>
        <header className="header">
          <nav className="nav">
            <div className="cart">
              My Cart(
                { totalQuantity(cartQuantity) || 0 } 
              )
            </div>
            <div className="cart-info">
              { cartInfo || 'Your Cart is empet' }
            </div>
          </nav>
        </header>
      </div>
    );
  }
}
