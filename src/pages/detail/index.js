import React, { Component } from 'react';
import image from '../../img/classic-tee.jpg';
import './detail.css';

export default class Detail extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      size: null,
      item: {
        name: 'Classic Tee',
        price: '$75.00'
      }
    };
  }

  handleClick(e) {
    this.setState({
      size: e.target.value
    });
  }

  render() {
    const currentSize = this.state.size;
    const { setCart } = this.props;
    return (
      <div className="container">
        <div className="preview">
          <img src={image} alt="" />
        </div>
        <div className="details">
          <h3 className="title">{this.state.item.name}</h3>
          <span className="price">{this.state.item.price}</span>
          <div className="dec">
            Dolor sit amet, consectetur adipiscing elit. Haec et tu ita
            posuisti, et verba vester sunt. Quod autem ratione actim est, id
            officium appellamus dolor sit amet, consectetur adipiscing elit.
            Haec ettu ita posuisti, et verba vester sunt. Quod autem ratione
            actim est, id officium appellamus dolor sit amet, consectetur
            adipiscing elit.
          </div>
          <span className="size">
            <label>SIZE</label>
          </span>
          <div className="size-type">
            <ul className="cart-box">
              <li>
                <label className={currentSize === 'S' ? 'checked' : ''}>
                  <input
                    type="radio"
                    value="S"
                    checked={currentSize === 'S'}
                    onChange={this.handleClick}
                  />
                  S
                </label>
              </li>
              <li>
                <label className={currentSize === 'M' ? 'checked' : ''}>
                  <input
                    type="radio"
                    value="M"
                    checked={currentSize === 'M'}
                    onChange={this.handleClick}
                  />
                  M
                </label>
              </li>
              <li>
                <label className={currentSize === 'L' ? 'checked' : ''}>
                  <input
                    type="radio"
                    value="L"
                    checked={currentSize === 'L'}
                    onChange={this.handleClick}
                  />
                  L
                </label>
              </li>
            </ul>
          </div>
          <button
            className="btn"
            onClick={() => setCart({ size: this.state.size })}
          >
            ADD TO CART
          </button>
        </div>
      </div>
    );
  }
}
